package uz.azn.alarmclock

import android.content.Context
import android.media.MediaParser
import android.media.MediaPlayer
import android.os.*
import android.provider.AlarmClock
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import androidx.core.view.isVisible
import com.google.android.material.button.MaterialButton
import uz.azn.alarmclock.R.id.*
import java.util.*


class FragmentClock : Fragment() {

    private var progressSeekbar = 0

    lateinit var mediaPlayer: MediaPlayer
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_clock, container, false)
        val clockText = view.findViewById<TextView>(clock_text_view)
        val seekBar = view.findViewById<SeekBar>(seek_bar)
        val startButton = view.findViewById<Button>(start_btn)
        val pauseButton = view.findViewById<Button>(pause_btn)
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val seekString = seekBar?.progress
                clockText.text = String.format(Locale.getDefault(), "%02d:%02d", seekString, 0)
                progressSeekbar = seekString!!
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        startButton.setOnClickListener {

            object : CountDownTimer((progressSeekbar * 1000 * 60).toLong(), 1000) {
                override fun onFinish() {
                    mediaPlayer = MediaPlayer.create(context, R.raw.banana_fon)

                    if (seekBar.progress > 0) {
                        startButton.visibility = View.INVISIBLE
                        seekBar.visibility = View.INVISIBLE
                        pauseButton.visibility = View.VISIBLE
                        seekBar.progress = 0
                        vibrate()
                        mediaPlayer.start()



                    }
                }

                override fun onTick(millisUntilFinished: Long) {
                    val a = millisUntilFinished
                    val number = a.toInt()
                    val minuts = number / 1000 / 60
                    val second = number / 1000 % 60
                    val timeLeftFormatted =
                        String.format(Locale.getDefault(), "%02d:%02d", minuts, second)
                    clockText.text = timeLeftFormatted
                }
            }.start()
        }

        pauseButton.setOnClickListener {
            startButton.visibility = View.VISIBLE
            seekBar.visibility = View.VISIBLE
            mediaPlayer.pause()
            pauseButton.visibility = View.INVISIBLE
        }
        return view
    }
    private fun vibrate() {
        val vibrator = requireContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator.vibrate(1000)
        }
    }

}